# Generated by Django 4.1.7 on 2023-03-29 11:23

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('airline_api', '0004_rename_flight_booking_flight_id_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='confirmed',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='booking',
            name='payment_id',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='flight',
            name='arrival_time',
            field=models.TimeField(default=datetime.datetime(2023, 3, 29, 11, 23, 15, 540334, tzinfo=datetime.timezone.utc)),
        ),
        migrations.AlterField(
            model_name='flight',
            name='date',
            field=models.DateField(default=datetime.datetime(2023, 3, 29, 11, 23, 15, 540334, tzinfo=datetime.timezone.utc)),
        ),
        migrations.AlterField(
            model_name='flight',
            name='departure_time',
            field=models.TimeField(default=datetime.datetime(2023, 3, 29, 11, 23, 15, 540334, tzinfo=datetime.timezone.utc)),
        ),
    ]
