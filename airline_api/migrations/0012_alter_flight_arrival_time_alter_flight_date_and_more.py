# Generated by Django 4.1.7 on 2023-05-06 12:02

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('airline_api', '0011_alter_flight_arrival_time_alter_flight_date_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='flight',
            name='arrival_time',
            field=models.TimeField(default=datetime.datetime(2023, 5, 6, 12, 2, 0, 344485, tzinfo=datetime.timezone.utc)),
        ),
        migrations.AlterField(
            model_name='flight',
            name='date',
            field=models.DateField(default=datetime.datetime(2023, 5, 6, 12, 2, 0, 344485, tzinfo=datetime.timezone.utc)),
        ),
        migrations.AlterField(
            model_name='flight',
            name='departure_time',
            field=models.TimeField(default=datetime.datetime(2023, 5, 6, 12, 2, 0, 344485, tzinfo=datetime.timezone.utc)),
        ),
    ]
