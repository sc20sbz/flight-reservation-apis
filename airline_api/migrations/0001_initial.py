# Generated by Django 3.1 on 2023-03-11 09:17

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Flight',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('flight_id', models.IntegerField()),
                ('airline', models.CharField(max_length=100)),
                ('departure_airport', models.CharField(max_length=100)),
                ('arrival_airport', models.CharField(max_length=100)),
                ('date', models.DateField()),
                ('departure_time', models.TimeField()),
                ('arrival_time', models.TimeField()),
            ],
        ),
    ]
