from django.contrib import admin
from .models import booking, user, flight
# Register your models here.

admin.site.register(booking)
admin.site.register(user)
admin.site.register(flight)