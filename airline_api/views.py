#Reference
#Status code : https://www.django-rest-framework.org/api-guide/status-codes/
#icontains : https://www.w3schools.com/django/ref_lookups_icontains.php 
#error handling : https://studygyaan.com/django/erro-handling?utm_content=cmp-true

from django.shortcuts import render
from airline_api.models import flight,booking,user
from rest_framework.response import Response
from rest_framework.decorators import api_view
from datetime import datetime
from django.shortcuts import get_object_or_404
from rest_framework import status

from airline_api.serializer import flightSerializer, bookingSerializer


#local host - http://127.0.0.1:8000/api/searchFlight/?departure_location=London&arrival_location=Los%20Angeles&depature_date=2023-06-01
#deployed - http://sc20sbz.pythonanywhere.com/api/searchFlight/?departure_location=London&arrival_location=Los%20Angeles&depature_date=2023-06-01
@api_view(['GET'])
def searchFlight(request):
    try:
        #get depature airport
        departure_airport = request.query_params.get('departure_location')
        #print(departure_airport)
        #get arrival airport
        arrival_airport = request.query_params.get('arrival_location')
        #get date 
        date = request.query_params.get('depature_date')

        if not all([departure_airport, arrival_airport, date]):
            return Response('Missing required informations.', status=status.HTTP_400_BAD_REQUEST)
        #replace / to - in case the date is in 'YYYY/MM/DD' format
        date = date.replace("/", "-")
        date = datetime.strptime(date, '%Y-%m-%d')

        #Reference : https://www.w3schools.com/django/ref_lookups_icontains.php 
        flights = flight.objects.filter(
            departure_airport__icontains=departure_airport,
            arrival_airport__icontains=arrival_airport,
            date=date
        )
        #use serializer to return data
        serializer = flightSerializer(flights, many=True)
        return Response(serializer.data,  status=status.HTTP_200_OK)

    except ValueError:
        return Response('No flight available, please change departure airport, arrival airport and date', status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        return Response(str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    

#local host- http://127.0.0.1:8000/api/bookFlight/?flight_id=1&date=2023-05-30&first_name=suriana&last_name=soh&email=surianasoh@gmail.com
@api_view(['GET','POST'])
def createBooking(request):
    try: 
        flight_id = request.query_params.get('flight_id')
        date = request.query_params.get('date')
        first_name = request.query_params.get('first_name')
        last_name = request.query_params.get('last_name')
        email = request.query_params.get('email')
        if not all([flight_id, date, first_name, last_name, email]):
            return Response('Missing required informations.', status=status.HTTP_400_BAD_REQUEST)
    
         #change the date format to ensure it is correct
        date = date.replace("/", "-")
        date = datetime.strptime(date, '%Y-%m-%d')
        flights = flight.objects.filter(
        id=flight_id,
        date = date
        )
        serializer = flightSerializer(flights, many=True)
        print(serializer.data)
        flights = flight.objects.get(id=flight_id,date = date)

        #check if flight actually exist before making booking
        if len(serializer.data)> 0 and flights.no_of_seats > 0 : 
            flights.no_of_seats -= 1
            flights.save()
            try:
                newuser = user.objects.get(email=email, first_name=first_name, last_name=last_name)
                user_id = newuser.id

            except user.DoesNotExist: 
                newuser = user(email=email, first_name=first_name, last_name=last_name)
                newuser.save()
                user_id = newuser.id

            users = user.objects.filter(
            id=user_id
            )
            # Create the booking object and save it to the database
            newbooking = booking( user_id=user_id, reference_id=flight_id + str(user_id), flight_id=flight_id,)
            newbooking.save()

            serializer = bookingSerializer(newbooking)            
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else :
            return Response('No flight available', status=status.HTTP_400_BAD_REQUEST)

    except ValueError:
        return Response('No flight available', status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        return Response(str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    


#http://sc20sbz.pythonanywhere.com/api/editBooking/?reference_id=54&last_name=Mob&new_first_name=Mary&new_last_name=Mob
@api_view(['GET','PUT'])
def editBooking(request):
    try:
        reference_id = request.query_params.get('reference_id')
        last_name = request.query_params.get('last_name')
        new_first_name = request.query_params.get('new_first_name')
        new_last_name = request.query_params.get('new_last_name')

        if not all([reference_id, last_name, new_first_name, new_last_name]):
            return Response('Missing required information.', status=status.HTTP_400_BAD_REQUEST)

        try:
            edituser = user.objects.get(last_name= last_name)
        except edituser.DoesNotExist:
            return Response('User not found.', status=status.HTTP_400_BAD_REQUEST)

        edituser.first_name = new_first_name
        edituser.last_name = new_last_name
        edituser.save()

        return Response('Booking has been updated.', status=status.HTTP_200_OK)
    except ValueError:
        return Response('No such booking', status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        return Response(str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    



# /api/cancelBooking?reference_id=64&last_name=soh
# deployed- http://sc20sbz.pythonanywhere.com/api/cancelBooking/?reference_id=54&last_name=Mob
@api_view(['GET','DELETE'])
def cancelBooking(request):
    try:
        reference_id = request.query_params.get('reference_id')
        last_name = request.query_params.get('last_name')
        if not last_name:
            return Response('Last name is required.', status=400)
        deletebooking = booking.objects.get(reference_id=reference_id)
        increaseflight = flight.objects.get(id=deletebooking.flight_id)
        increaseflight.no_of_seats += 1
        increaseflight.save()
        deletebooking.delete()
        return Response('Booking has been cancelled.', status=200)
    
    except ValueError:
        return Response('No such booking', status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        return Response(str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    




#deployed - http://sc20sbz.pythonanywhere.com/api/getBooking/?reference_id=64&last_name=soh
@api_view(['GET'])
def getBooking(request):
    try:
        reference_id = request.query_params.get('reference_id')
        if not reference_id:
            return Response('Reference id is required.', status=400)
        booking_obj = booking.objects.get(reference_id=reference_id)
        serializer = bookingSerializer(booking_obj)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except ValueError:
        return Response('No such booking', status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        return Response(str(e), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    



#http://sc20sbz.pythonanywhere.com/api/confirmPayment/?reference_id=64&payment_id=92992929
@api_view(['GET', 'POST'])
def confirmPayment(request):
    reference_id = request.query_params.get('reference_id')
    payment_id = request.query_params.get('payment_id')

    try:
        updatebooking = booking.objects.get(reference_id=reference_id)
        updatebooking.confirmed = 1
        updatebooking.save()
    except updatebooking.DoesNotExist:
        return Response('Booking not found.', status=status.HTTP_400_BAD_REQUEST)

    

    increaseflight = flight.objects.get(id=updatebooking.flight_id)
    increaseflight.no_of_seats -=1
    increaseflight.save()

   

    return Response('Payment id has been updated.', status=status.HTTP_200_OK)






