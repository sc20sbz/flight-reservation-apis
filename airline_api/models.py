from django.db import models
from django.utils import timezone
from datetime import timedelta


# Create your models here.
class flight(models.Model):
    flight_id = models.CharField(max_length=100)
    airline = models.CharField(max_length=100)
    departure_airport = models.CharField(max_length=100,default="unspecified")
    arrival_airport = models.CharField(max_length=100,default="unspecified")
    date = models.DateField(default=timezone.now())
    departure_time=models.TimeField(default=timezone.now())
    arrival_time=models.TimeField(default=timezone.now())
    price = models.IntegerField(default=100)
    no_of_seats = models.IntegerField(default= 100)
    duration = models.IntegerField(default=10)
 

class booking(models.Model):
    reference_id = models.CharField(max_length=20, unique=True)
    user = models.ForeignKey('user', on_delete=models.CASCADE)
    flight = models.ForeignKey('flight', on_delete=models.CASCADE)
    payment_id = models.IntegerField(default=0)
    confirmed = models.IntegerField(default=0)

    def __str__(self):
        return ("%s" % (self.reference_id))


class user(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_no = models.CharField(max_length=20)
    email = models.EmailField(max_length=100)

    def __str__(self):
        return ("%s %s" % (self.first_name, self.last_name))



    



