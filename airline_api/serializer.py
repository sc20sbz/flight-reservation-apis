from rest_framework import serializers
from airline_api.models import flight, user, booking

class flightSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    airline = serializers.CharField()
    departure_airport = serializers.CharField()
    arrival_airport = serializers.CharField()
    date = serializers.DateField()
    departure_time=serializers.TimeField()
    arrival_time=serializers.TimeField()
    price =serializers.IntegerField()

class user(serializers.Serializer):
    first_name = serializers.CharField(max_length=100)
    last_name = serializers.CharField(max_length=100)
    phone_no = serializers.CharField(max_length=20)
    email = serializers.EmailField(max_length=100)


class bookingSerializer(serializers.Serializer): 
    reference_id = serializers.CharField()
    flight_id = serializers.IntegerField(source='flight.id')
    date = serializers.DateField(source='flight.date')
    first_name = serializers.CharField(source='user.first_name')
    last_name =serializers.CharField(source='user.last_name')
    price = serializers.IntegerField(source='flight.price')
    confirmed = serializers.IntegerField()
