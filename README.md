# Flight Reservation APIs

## Description
### Requirements
The airline web services should manage provide flights information.
The airline web services should create bookings.
The airline web services should edit bookings.
The airline web services should manage cancellations.
The airline web services should confirm payment.

The project is about implementing airline web service that allowed the flight aggregator applications to find most suitable flights, book a seat on the flight and manage customers bookings. Although the implementation could be improved in a number of ways, it is critical to adhere to the technical specifications developed in coursework 1 to ensure that the code communicates effectively with the rest of the system. The following credentials will allow user to login as admin.

username: ammar
password: ammar123

The name of pythonanywhere domain- http://sc20sbz.pythonanywhere.com/

## Authors and acknowledgment
Suriana University of Leeds

## Project status
Project has been successfully completed